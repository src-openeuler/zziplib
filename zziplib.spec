Name:            zziplib
Version:         0.13.78
Release:         3
Summary:         Lightweight library for zip compression
License:         LGPL-2.0-or-later OR MPL-1.1
URL:             https://zziplib.sourceforge.net
Source0:         https://github.com/gdraheim/zziplib/archive/refs/tags/v%{version}.tar.gz
Patch0:          zziplib-0.13.78-fix-pkgconfig-pkgname.patch
Patch1:          zziplib-0.13.78-port-to-newer-cmake.patch
BuildRequires:   gcc make
BuildRequires:   cmake >= 3.12
BuildRequires:   zlib-devel
BuildRequires:   pkgconfig(sdl2)

Provides:        zziplib-utils = %{version}-%{release}
Obsoletes:       zziplib-utils < %{version}-%{release}

%description
The zziplib is a lightweight library to easily extract data from zip files. Applications
can bundle files into a single zip archive and access them. The implementation is based
only on the (free) subset of compression with the zlib algorithm which is actually used
by the zip/unzip tools.

%package         devel
Summary:         Header files and libraries for zziplib development
Requires:        %{name} = %{version}-%{release}

%description     devel
This package contains the header files and libraries needed to
develop programs that use the zziplib compression and decompression
library.

%package_help

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%cmake -DZZIP_TESTCVE=OFF
%cmake_build

%install
%cmake_install

%files
%license docs/COPYING.LIB docs/COPYING.MPL
%doc ChangeLog README TODO
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%doc docs/README.SDL docs/*.htm
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/*.m4
%{_libdir}/cmake/*

%files help
%{_mandir}/man3/*

%changelog
* Tue Mar 04 2025 Funda Wang <fundawang@yeah.net> - 0.13.78-3
- build with cmake 4.0

* Thu Nov 28 2024 Funda Wang <fundawang@yeah.net> - 0.13.78-2
- build with sdl2 now

* Wed Aug 21 2024 Funda Wang <fundawang@yeah.net> - 0.13.78-1
- update to 0.13.78

* Mon Aug 12 2024 wangxiaomeng <wangxiaomeng@kylinos.cn> - 0.13.77-1
- update to 0.13.77
  - make afl to check for fuzzer bugs
  - update os versions to latest from docker_mirror.py
  - add missing tests scenarios for later os releases
  - fix Coverage include hack
  - integrate mxe/src/zziplib-2-prefer-win32-mmap.patch
  - make crossgcc/windows a working example for mingw
  - fix dbk2man regression (from typehints changes)

* Mon Aug 12 2024 baiguo <baiguo@kylinos.cn> - 0.13.74-3
- fix CVE-2024-39133

* Fri Aug 9 2024 baiguo <baiguo@kylinos.cn> - 0.13.74-2
- fix CVE-2024-39134

* Tue Jul  9 2024 dillon chen <dillon.chen@gmail.com> - 0.13.74-1
- update to 0.13.74

* Wed Jul  5 2023 dillon chen <dillon.chen@gmail.com> - 0.13.72-2
- add -DZZIP_TESTCVE=OFF skip download test(curl github)

* Tue Sep 27 2022 dillon chen <dillon.chen@gmail.com> - 0.13.72-1
- update to 0.13.72

* Sat Sep 04 2021 shixuantong <shixuantong@huawei.com> - 0.13.71-3
- remove rpath

* Fri Jun 25 2021 shixuantong <shixuantong@huawei.com> - 0.13.71-2
- fix CVE-2020-18442

* Tue Nov 3 2020 tianwei <tianwei12@huawei.com> - 0.13.71-1
- update to 0.13.71 and remove python2

* Fri Feb 14 2020 chengquan <chengquan3@huawei.com> - 0.13.36-5
- Add necessary BuildRequires

* Thu Jan 9 2020 BruceGW <gyl93216@163.com> - 0.13.36-4
- Delete useless patch

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.13.36-3
- Fix CVE-2018-16548 and CVE-2018-17828

* Thu Sep 12 2019 dongjian <dongjian13@huawei.com> 0.13.36-2
- Modification summary
